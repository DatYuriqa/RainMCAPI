package net.rainmc.service;

import lombok.Getter;

import java.util.HashMap;

public class ServiceProvider {

    private final @Getter HashMap<String, IProvider<?>> providers;
    private final @Getter HashMap<String, IService<?>> services;

    public ServiceProvider() {
        this.providers = new HashMap<>();
        this.services = new HashMap<>();
    }

    public void registerProvider(IProvider<?> provider) {
        this.providers.put(provider.getName(), provider);
    }

    public void unregisterProvider(IProvider<?> provider) {
        this.providers.remove(provider.getName(), provider);
    }

    public IProvider<?> getProvider(String name) {
        return this.providers.get(name.toLowerCase());
    }

    public void registerService(IService<?> service) {
        this.services.put(service.getName(), service);
    }

    public void unregisterService(IService<?> service) {
        this.services.remove(service.getName(), service);
    }

    public IService<?> getService(String name) {
        return this.services.get(name.toLowerCase());
    }



}
