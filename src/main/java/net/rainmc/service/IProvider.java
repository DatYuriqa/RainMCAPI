package net.rainmc.service;

public interface IProvider<C> {

    String getName();

}
