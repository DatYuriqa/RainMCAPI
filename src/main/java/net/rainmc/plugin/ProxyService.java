package net.rainmc.plugin;

import lombok.Getter;
import net.rainmc.api.AbstractProxyPlugin;
import net.rainmc.api.async.AsyncProvider;
import net.rainmc.api.cooldown.CooldownProvider;
import net.rainmc.api.currency.CurrencyProvider;
import net.rainmc.api.currency.ICustomCurrency;
import net.rainmc.api.enums.ConfigType;
import net.rainmc.api.file.ConfigProvider;
import net.rainmc.api.file.IPluginConfig;
import net.rainmc.api.message.MySQLMessageService;
import net.rainmc.api.mysql.MySQLClient;
import net.rainmc.api.mysql.MySQLProvider;
import net.rainmc.api.mysql.PreparedStatementBuilder;
import net.rainmc.api.player.PlayerProvider;
import net.rainmc.api.scheduler.SchedulerService;
import net.rainmc.plugin.commands.proxy.InternalProxyPluginCommand;
import net.rainmc.plugin.manager.ProxyPluginManager;
import net.rainmc.service.ServiceProvider;

public final class ProxyService extends AbstractProxyPlugin {

    public static ProxyService instance;

    private @Getter ServiceProvider serviceProvider;
    private @Getter ProxyPluginManager pluginManager;

    @Override
    public void onEnable() {
        instance = this;
        this.serviceProvider = new ServiceProvider();
        this.pluginManager = new ProxyPluginManager();

        getProxy().getConsole().sendMessage("    ____        _       __  _________");
        getProxy().getConsole().sendMessage("   / __ \\____ _(_)___  /  |/  / ____/");
        getProxy().getConsole().sendMessage("  / /_/ / __ `/ / __ \\/ /|_/ / /     ");
        getProxy().getConsole().sendMessage(" / _, _/ /_/ / / / / / /  / / /___   ");
        getProxy().getConsole().sendMessage("/_/ |_|\\__,_/_/_/ /_/_/  /_/\\____/   ");
        getProxy().getConsole().sendMessage(" ");

        AsyncProvider asyncProvider = new AsyncProvider();
        CooldownProvider cooldownProvider = new CooldownProvider(serviceProvider);
        ConfigProvider configProvider = new ConfigProvider();
        PlayerProvider playerProvider = new PlayerProvider(serviceProvider);
        MySQLProvider mySQLProvider = new MySQLProvider();
        CurrencyProvider currencyProvider = new CurrencyProvider();

        this.serviceProvider.registerProvider(asyncProvider);
        this.serviceProvider.registerProvider(cooldownProvider);
        this.serviceProvider.registerProvider(configProvider);
        this.serviceProvider.registerProvider(playerProvider);
        this.serviceProvider.registerProvider(mySQLProvider);
        this.serviceProvider.registerProvider(currencyProvider);

        SchedulerService schedulerService = new SchedulerService(serviceProvider);
        IPluginConfig sqlConfig = configProvider.factory().createMySQLConfig(ConfigType.PROXY, this.getDataFolder().toString(), "mysql.yml");
        MySQLClient client = mySQLProvider.newConnectionPool(this, sqlConfig);
        client.update(new PreparedStatementBuilder("CREATE TABLE IF NOT EXISTS LANGUAGE (UUID VARCHAR(64), " +
                "LANG VARCHAR(64))", client).build());

        MySQLMessageService mySQLMessageService = new MySQLMessageService(client);

        ICustomCurrency coins = currencyProvider.registerCurrency("coins", client);

        this.serviceProvider.registerService(schedulerService);
        this.serviceProvider.registerService(mySQLMessageService);

        this.serviceProvider.getProviders().forEach((s, iProvider) -> {
            this.getProxy().getConsole().sendMessage("§7Der Provider: §c" + s + " §7wurde im System §aregistriert§7.");
        });

        this.serviceProvider.getServices().forEach((s, iProvider) -> {
            this.getProxy().getConsole().sendMessage("§7Der Service: §c" + s + " §7wurde im System §aregistriert§7.");
        });

        this.getProxy().getConsole().sendMessage(" ");

        getProxy().getPluginManager().registerCommand(this, new InternalProxyPluginCommand());

        super.onEnable();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}
