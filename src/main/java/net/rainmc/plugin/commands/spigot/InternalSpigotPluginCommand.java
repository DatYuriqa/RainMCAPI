package net.rainmc.plugin.commands.spigot;

import net.rainmc.plugin.SpigotService;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class InternalSpigotPluginCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(!(commandSender instanceof Player)) {
            Bukkit.getConsoleSender().sendMessage("§7Im System registrierte Plugins:");
            SpigotService.instance.getPluginManager().getPlugins().forEach((s1, plugin) -> {
                Bukkit.getConsoleSender().sendMessage("-" + s1);
            });
        }

        return false;
    }
}
