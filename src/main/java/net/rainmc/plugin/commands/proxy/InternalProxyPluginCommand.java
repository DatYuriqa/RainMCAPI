package net.rainmc.plugin.commands.proxy;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.rainmc.plugin.ProxyService;

public class InternalProxyPluginCommand extends Command {

    public InternalProxyPluginCommand() {
        super("ipl");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if(!(commandSender instanceof ProxiedPlayer)) {
            ProxyService.instance.getProxy().getConsole().sendMessage("§7Im System registrierte Plugins:");
            ProxyService.instance.getPluginManager().getPlugins().forEach((s1, plugin) -> {
                ProxyService.instance.getProxy().getConsole().sendMessage("-" + s1);
            });
        }
    }
}
