package net.rainmc.plugin.manager;

import lombok.Getter;
import net.rainmc.api.AbstractSpigotPlugin;
import org.bukkit.Bukkit;

import java.util.HashMap;

public class SpigotPluginManager {

    private final @Getter HashMap<String, AbstractSpigotPlugin> plugins;

    public SpigotPluginManager() {
        this.plugins = new HashMap<>();
    }

    public void register(AbstractSpigotPlugin plugin) {
        this.plugins.put(plugin.getName(), plugin);
        Bukkit.getConsoleSender().sendMessage("§7Das Plugin: §c" + plugin.getName()
                + " §7wurde im System §aregistriert§7.");
    }

    public void unregister(AbstractSpigotPlugin plugin) {
        this.plugins.remove(plugin.getName(), plugin);
        Bukkit.getConsoleSender().sendMessage("§7Das Plugin: §c" + plugin.getName()
                + " §7wurde im System §centregistriert§7.");
    }

    public void disable(AbstractSpigotPlugin plugin) {
        Bukkit.getPluginManager().disablePlugin(plugin);
        Bukkit.getConsoleSender().sendMessage("§7Das Plugin: §c" + plugin.getName()
                + " §7wurde im System §aregistriert§7.");
    }

    public void enable(AbstractSpigotPlugin plugin) {
        Bukkit.getPluginManager().enablePlugin(plugin);
        Bukkit.getConsoleSender().sendMessage("§7Das Plugin: §c" + plugin.getName()
                + " §7wurde im System §centregistriert§7.");
    }

    public AbstractSpigotPlugin getPlugin(AbstractSpigotPlugin plugin) {
        return this.plugins.get(plugin.getName());
    }



}
