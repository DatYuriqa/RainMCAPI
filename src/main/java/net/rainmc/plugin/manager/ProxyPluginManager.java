package net.rainmc.plugin.manager;

import lombok.Getter;
import net.rainmc.api.AbstractProxyPlugin;

import java.util.HashMap;

public class ProxyPluginManager {

    private final @Getter HashMap<String, AbstractProxyPlugin> plugins;

    public ProxyPluginManager() {
        this.plugins = new HashMap<>();
    }

    public void register(AbstractProxyPlugin plugin) {
        this.plugins.put(plugin.getProxy().getName(), plugin);
        plugin.getProxy().getConsole().sendMessage("§7Das Plugin: §c" + plugin.getProxy().getName()
                + " §7wurde im System §aregistriert§7.");
    }

    public void unregister(AbstractProxyPlugin plugin) {
        this.plugins.remove(plugin.getProxy().getName(), plugin);
        plugin.getProxy().getConsole().sendMessage("§7Das Plugin: §c" + plugin.getProxy().getName()
                + " §7wurde im System §centregistriert§7.");
    }

    public AbstractProxyPlugin getPlugin(AbstractProxyPlugin plugin) {
        return this.plugins.get(plugin.getProxy().getName());
    }

}
