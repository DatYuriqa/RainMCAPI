package net.rainmc.api.scheduler;

import net.rainmc.api.async.AsyncProvider;
import net.rainmc.service.IService;
import net.rainmc.service.ServiceProvider;

import java.util.Timer;
import java.util.TimerTask;

public final class SchedulerService implements IService<SchedulerService> {

    private final AsyncProvider asyncProvider;

    public SchedulerService(ServiceProvider serviceProvider) {
        this.asyncProvider = (AsyncProvider) serviceProvider.getProvider("async");
    }

    public void delay(Runnable runnable, int seconds) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runnable.run();
            }
        }, seconds * 1000L);
    }

    public void repeat(Runnable runnable, int delay, int period) {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runnable.run();
            }
        }, delay * 1000L, period * 1000L);
    }

    public void delayAsync(Runnable runnable, int seconds) {
        this.asyncProvider.newAsyncTask(() ->{
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    runnable.run();
                }
            }, seconds * 1000L);
        });
    }

    public void repeatAsync(Runnable runnable, int delay, int period) {
        this.asyncProvider.newAsyncTask(() ->{
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    runnable.run();
                }
            }, delay * 1000L, period * 1000L);
        });
    }

    @Override
    public String getName() {
        return "scheduler";
    }
}
