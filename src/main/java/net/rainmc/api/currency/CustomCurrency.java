package net.rainmc.api.currency;

import net.rainmc.api.player.documents.PlayerCurrencyDocument;

import java.util.UUID;

public class CustomCurrency implements ICustomCurrency {

    private final String name;
    private final MySQLCurrency currency;

    public CustomCurrency(String name, MySQLCurrency currency) {
        this.name = name;
        this.currency = currency;
    }

    @Override
    public void add(UUID uuid, int amount) {
        PlayerCurrencyDocument document = this.currency.getPlayer(uuid);
        document.setAmount(document.getAmount() + amount);
    }

    @Override
    public void remove(UUID uuid, int amount) {
        PlayerCurrencyDocument document = this.currency.getPlayer(uuid);
        document.setAmount(document.getAmount() - amount);
    }

    @Override
    public void set(UUID uuid, int amount) {
        PlayerCurrencyDocument document = this.currency.getPlayer(uuid);
        document.setAmount(amount);
    }

    @Override
    public int get(UUID uuid) {
        PlayerCurrencyDocument document = this.currency.getPlayer(uuid);
        return document.getAmount();
    }

    @Override
    public String getName() {
        return this.name;
    }
}
