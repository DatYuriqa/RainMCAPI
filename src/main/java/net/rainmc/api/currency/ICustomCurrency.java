package net.rainmc.api.currency;

import java.util.UUID;

public interface ICustomCurrency {

    void add(UUID uuid, int amount);

    void remove(UUID uuid, int amount);

    void set(UUID uuid, int amount);

    int get(UUID uuid);

    String getName();

}
