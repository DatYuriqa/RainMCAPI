package net.rainmc.api.currency;

import net.rainmc.api.mysql.MySQLClient;
import net.rainmc.service.IProvider;

import java.util.HashMap;

public final class CurrencyProvider implements IProvider<CurrencyProvider> {

    private final HashMap<String, ICustomCurrency> currencies;

    public CurrencyProvider() {
        this.currencies = new HashMap<>();
    }

    public ICustomCurrency registerCurrency(String name, MySQLClient client) {
        if(!this.currencies.containsKey(name)) {
            MySQLCurrency mySQLCurrency = new MySQLCurrency(name, client);
            ICustomCurrency currency = new CustomCurrency(name, mySQLCurrency);
            this.currencies.put(name, currency);
        }
        return this.currencies.get(name);
    }

    public ICustomCurrency getCurrency(String name) {
        return this.currencies.get(name);
    }

    @Override
    public String getName() {
        return "currency";
    }
}
