package net.rainmc.api.currency;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import net.rainmc.api.mysql.IMySQLCallback;
import net.rainmc.api.mysql.MySQLClient;
import net.rainmc.api.mysql.PreparedStatementBuilder;
import net.rainmc.api.player.documents.PlayerCurrencyDocument;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class MySQLCurrency {

    private MySQLClient client;

    private String name;

    public LoadingCache<UUID, PlayerCurrencyDocument> cache = CacheBuilder.newBuilder().build(new CacheLoader<UUID, PlayerCurrencyDocument>() {
        @Override
        public PlayerCurrencyDocument load(UUID uuid) throws Exception {
            return loadPlayer(uuid);
        }
    });

    public MySQLCurrency(String name, MySQLClient client) {
        this.client = client;
        this.name = name;

        this.client.update(new PreparedStatementBuilder("CREATE TABLE IF NOT EXISTS " + name
                + " (UUID VARCHAR(64), AMOUNT INT(64))", client).build());
    }

    public PlayerCurrencyDocument getPlayer(UUID uuid) {
        return this.cache.getUnchecked(uuid);
    }

    public PlayerCurrencyDocument loadPlayer(UUID uuid) {
        this.client.update(new PreparedStatementBuilder("INSERT IGNORE INTO " + this.name
                + " (UUID, AMOUNT) VALUES (?, ?)", this.client).bindString(uuid.toString()).bindInt(0).build());

        PlayerCurrencyDocument document = new PlayerCurrencyDocument();

        this.client.update(new PreparedStatementBuilder("SELECT * FROM " + this.name + " WHERE UUID=?",
                this.client).bindString(uuid.toString()).build(), new IMySQLCallback<ResultSet>() {
            @Override
            public void success(ResultSet result) throws SQLException {
                document.setUuid(uuid);
                document.setAmount(result.getInt("AMOUNT"));
            }

            @Override
            public void failed(Throwable throwable) {

            }
        });
        return document;
    }

    public void update(UUID uuid) {
        PlayerCurrencyDocument document = this.cache.getUnchecked(uuid);

        this.client.update(new PreparedStatementBuilder("UPDATE " + this.name + " SET AMOUNT=? WHERE UUID=?",
                this.client).bindInt(document.getAmount()).bindString(uuid.toString()).build());

        this.cache.invalidate(uuid);
    }

}
