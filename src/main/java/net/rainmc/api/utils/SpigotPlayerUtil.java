package net.rainmc.api.utils;

import de.dytanic.cloudnet.api.CloudAPI;
import de.dytanic.cloudnet.api.player.PlayerExecutorBridge;
import de.dytanic.cloudnet.lib.player.CloudPlayer;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;

import java.util.stream.Stream;

public class SpigotPlayerUtil {

    public static void resetPlayer(Player player) {
        player.getInventory().clear();;
        player.getInventory().setArmorContents(null);

        player.setHealth(player.getMaxHealth());
        player.setFoodLevel(20);
        player.setLevel(0);
        player.setExp(0);
        player.setFireTicks(0);
        player.setAllowFlight(false);
        player.setFlying(false);
        player.setGameMode(Bukkit.getDefaultGameMode());
        player.spigot().setCollidesWithEntities(true);

        player.getActivePotionEffects().forEach(potionEffect -> {
            player.removePotionEffect(potionEffect.getType());
        });

    }

    public static void sendActionbar(Player player, String text) {
        IChatBaseComponent baseComponent = IChatBaseComponent.ChatSerializer.a("{ \"text\": \"" + text + "\" }");
        PacketPlayOutChat chatPacket = new PacketPlayOutChat(baseComponent, (byte) 2);

        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(chatPacket);

    }

    public static void sendTitle(Player player, String title, String subTitle, int fadeIn, int stay, int fadeOut) {
        IChatBaseComponent titleBaseComponent = IChatBaseComponent.ChatSerializer.a("{ \"text\": \"" + title + "\" }");
        PacketPlayOutTitle titlePacket = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, titleBaseComponent, fadeIn, stay, fadeOut);

        IChatBaseComponent subTitleBaseComponent = IChatBaseComponent.ChatSerializer.a("{ \"text\": \"" + subTitle + "\" }");
        PacketPlayOutTitle subTitlePacket = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, subTitleBaseComponent, fadeIn, stay, fadeOut);


        Stream.of(titlePacket, subTitlePacket)
                .forEach(((CraftPlayer) player).getHandle().playerConnection::sendPacket);
    }

    public static void sendTitle(Player player, String title, String subTitle, int stay) {
        sendTitle(player, title, subTitle, 0, stay, 0);
    }

    public static void sendTitle(Player player, String title, String subTitle) {
        sendTitle(player, title, subTitle, 0, 60, 0);
    }

    public static void sendToServer(Player player, String server) {
        CloudPlayer cloudPlayer = CloudAPI.getInstance().getOnlinePlayer(player.getUniqueId());
        PlayerExecutorBridge.INSTANCE.sendPlayer(cloudPlayer, server);
    }

}
