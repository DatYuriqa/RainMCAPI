package net.rainmc.api.enums;

public enum ProviderType {

    ASYNC,
    COOLDOWN,
    PLAYER,
    CONFIG,
    MYSQL,
    GAME_STATE,
    CURRENCY;

}
