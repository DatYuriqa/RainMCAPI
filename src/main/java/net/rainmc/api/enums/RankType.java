package net.rainmc.api.enums;

import lombok.Getter;

public enum RankType {

    ADMIN(100, "Admin", "§4", "§4Admin §8▏ §7", "§4Admin §8▏ §7"),
    MANAGER(95, "Manager", "§c", "§cManager §8▏ §7", "§cManager §8▏ §7"),
    SR_DEVELOPER(90, "SrDeveloper", "§b", "§bSrDeveloper §8▏ §7", "§bSrDev §8▏ §7"),
    DEVELOPER(85, "Developer", "§b", "§bDeveloper §8▏ §7", "§bDev §8▏ §7"),
    TEST_DEVELOPER(80, "TestDeveloper", "§b", "§bTDeveloper §8▏ §7", "§bTDev §8▏ §7"),
    CONTENT(75, "Content", "§3", "§3Content §8▏ §7", "§3Content §8▏ §7"),
    SR_STAFF(50, "SrStaff", "§9", "§9SrStaff §8▏ §7", "§9SrStaff §8▏ §7"),
    STAFF(55, "Staff", "§a", "§aStaff §8▏ §7", "§aStaff §8▏ §7"),
    TEST_STAFF(50, "TestStaff", "§e", "§eTStaff §8▏ §7", "§eTStaff §8▏ §7"),
    BUILDER(40, "Builder", "§2", "§2Builder §8▏ §7", "§2Builder §8▏ §7"),
    YOUTUBER(20, "YouTuber", "§5", "§5", "§5"),
    PREMIUM_PLUS(15, "Premiumplus", "§e", "§e", "§e"),
    PREMIUM(5, "Premium", "§6", "§6", "§6"),
    PLAYER(0, "Default", "§7", "§7", "§7");

    private final @Getter int rankId;

    private final @Getter String name;
    private final @Getter String color;
    private final @Getter String chatPrefix;
    private final @Getter String tabPrefix;

    RankType(int rankId, String name, String color, String chatPrefix, String tabPrefix) {
        this.rankId = rankId;
        this.name = name;
        this.color = color;
        this.chatPrefix = chatPrefix;
        this.tabPrefix = tabPrefix;
    }

}
