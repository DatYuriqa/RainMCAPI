package net.rainmc.api.enums;

import lombok.Getter;

public enum LanguageType {

    GERMAN("de_DE"),
    ENGLISH("en_EN");

    private final @Getter String lang;

    LanguageType(String lang) {
        this.lang = lang;
    }

}
