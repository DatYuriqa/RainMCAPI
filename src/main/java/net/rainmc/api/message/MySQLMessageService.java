package net.rainmc.api.message;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import net.rainmc.api.enums.LanguageType;
import net.rainmc.api.mysql.IMySQLCallback;
import net.rainmc.api.mysql.MySQLClient;
import net.rainmc.api.mysql.PreparedStatementBuilder;
import net.rainmc.api.player.documents.PlayerLanguageDocument;
import net.rainmc.service.IService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public final class MySQLMessageService implements IService<MySQLMessageService> {

    private final MySQLClient client;

    public LoadingCache<UUID, PlayerLanguageDocument> cache = CacheBuilder.newBuilder().build(new CacheLoader<UUID, PlayerLanguageDocument>() {
        @Override
        public PlayerLanguageDocument load(UUID uuid) throws Exception {
            return load(uuid);
        }
    });

    public MySQLMessageService(MySQLClient client) {
        this.client = client;
    }

    public PlayerLanguageDocument getPlayer(UUID uuid) {
        return this.cache.getUnchecked(uuid);
    }

    public PlayerLanguageDocument loadPlayer(UUID uuid) {
        this.client.update(new PreparedStatementBuilder("INSERT IGNORE INTO LANGUAGE (UUID, LANG) VALUES (?,?)",
                this.client).bindString(uuid.toString()).bindString(LanguageType.ENGLISH.getLang()).build());

        PlayerLanguageDocument document = new PlayerLanguageDocument();

        this.client.update(new PreparedStatementBuilder("SELECT * FROM LANGUAGE WHERE UUID=?", this.client).build(),
                new IMySQLCallback<ResultSet>() {
                    @Override
                    public void success(ResultSet result) throws SQLException {
                        document.setUuid(uuid);
                        document.setLang(result.getString("LANG"));
                    }

                    @Override
                    public void failed(Throwable throwable) {

                    }
                });
        return document;
    }

    public void update(UUID uuid) {

        PlayerLanguageDocument document = this.cache.getUnchecked(uuid);

        this.client.update(new PreparedStatementBuilder("UPDATE LANGUAGE SET LANG=? WHERE UUID=?",
                this.client).bindString(document.getLang()).bindString(uuid.toString()).build());


        this.cache.invalidate(uuid);
    }

    @Override
    public String getName() {
        return "mysql_message";
    }
}
