package net.rainmc.api.message;

import net.rainmc.api.player.player.LanguagePlayer;

public class MessageBuilder {

    private String message;

    private final MessageConfig config;

    private final LanguagePlayer languagePlayer;

    public MessageBuilder(MessageConfig config, LanguagePlayer languagePlayer) {
        this.config = config;
        this.languagePlayer = languagePlayer;

    }

    public MessageBuilder setMessagePath(String path) {
        if(this.config.getMessage(path) == null) {
            this.config.createMessage(path, "");
        }
        this.message = this.config.getMessage(path, languagePlayer.getLanguage());
        return this;
    }

    public MessageBuilder setMessagePath(String path, String defaultMessage) {
        if(this.config.getMessage(path) == null) {
            this.config.createMessage(path, defaultMessage);
        }
        this.message = this.config.getMessage(path, languagePlayer.getLanguage());
        return this;
    }

    public MessageBuilder addPrefix() {
        this.message = this.config.getMessage("main.Prefix") + this.message;
        return this;
    }


    public String build() {
        return message;
    }

}
