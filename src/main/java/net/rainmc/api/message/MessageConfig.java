package net.rainmc.api.message;

import net.rainmc.api.enums.LanguageType;
import net.rainmc.api.file.IPluginConfig;
import net.rainmc.api.player.player.LanguagePlayer;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class MessageConfig {

    private HashMap<String, String> cache;

    private final IPluginConfig config;

    public MessageConfig(IPluginConfig config) {
        this.config = config;

        this.config.set("config.main.Prefix", "§8× §9RainMC §8┃ §7");
        this.config.set("config.main.NoPerms", "§8× §9RainMC §8┃ §7Dazu hast du keine Rechte.");
        this.config.set("config.main.SyntaxError", "§8× §9RainMC §8┃ §7Fehlerhafte Eingabe.");
        this.config.set("config.main.PlayerOffline", "§8× §9RainMC §8┃ §7Dieser Spieler befindet sich nicht auf dem Netzwerk");
        this.config.set("config.main.PlayerUnknown", "§8× §9RainMC §8┃ §7Dieser Spieler war nie auf dem Netzwerk.");

        this.config.create();
    }

    public String getMessage(String path, String language) {
        if(!this.cache.containsKey("config." + path + "." + language)) {
            this.cache.put("config." + path + "." + language, getMessageFromFile("config." + path + "." + language));
        }
        return this.cache.get("config." + path + "." + language);
    }

    public String getMessage(String path) {
        if(!this.cache.containsKey("config." + path)) {
            this.cache.put("config." + path, getMessageFromFile("config." + path));
        }
        return this.cache.get("config." + path);
    }

    public void createMessage(String path, String defaultMessage) {
        for(LanguageType value : LanguageType.values()) {
            this.config.set("config." + path + "." + value.getLang(), defaultMessage);
            this.cache.put("config." + path + "." + value.getLang(), defaultMessage);
        }
        this.config.save();
    }

    private String getMessageFromFile(String path) {
        return this.config.getString(path);
    }

    //PATH: config.main.prefix: Prefix
    //PATH: config.messages.Help.de_DE: Help

    //CACHE: messages.Help, HELP



}
