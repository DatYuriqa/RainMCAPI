package net.rainmc.api.game;

import java.util.HashMap;

public class GamePhaseManager {

    private final HashMap<Integer, IGamePhase> gamePhases;

    private IGamePhase currentGamePhase;

    public GamePhaseManager() {
        this.gamePhases = new HashMap<>();
        this.currentGamePhase = null;
    }

    public void setGamePhase(int stateId) {
        if(this.currentGamePhase != null) {
            this.currentGamePhase.stop();
        }
        this.currentGamePhase = this.gamePhases.get(stateId);
        this.currentGamePhase.start();
    }

    public void stopGamePhase() {
        this.currentGamePhase.stop();
        this.currentGamePhase = null;
    }

    public void addGamePhase(int stateId, IGamePhase state) {
        this.gamePhases.put(stateId, state);
    }

    public boolean isGamePhase(int stateId) {
        return this.currentGamePhase.equals(this.gamePhases.get(stateId));
    }

    public IGamePhase getCurrentGamePhase() {
        return this.currentGamePhase;
    }


}
