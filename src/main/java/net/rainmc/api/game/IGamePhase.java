package net.rainmc.api.game;

public interface IGamePhase {

    void start();

    void stop();
    

}
