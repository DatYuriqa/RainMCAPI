package net.rainmc.api.game;

import net.rainmc.api.AbstractSpigotPlugin;
import net.rainmc.service.IProvider;

import java.util.HashMap;

public class GamePhaseProvider implements IProvider<GamePhaseProvider> {

    private final HashMap<String, GamePhaseManager> gameStateManagers;

    public GamePhaseProvider() {
        this.gameStateManagers = new HashMap<>();
    }

    public GamePhaseManager newGameStateManager(AbstractSpigotPlugin plugin) {
        if(!this.gameStateManagers.containsKey(plugin.getName())) {
            GamePhaseManager manager = new GamePhaseManager();
            this.gameStateManagers.put(plugin.getName(), manager);
        }
        return this.gameStateManagers.get(plugin.getName());
    }

    @Override
    public String getName() {
        return "game_state";
    }
}
