package net.rainmc.api.mysql;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import net.rainmc.api.file.IPluginConfig;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQLClient {

    private final HikariDataSource hikariDataSource;

    private Connection connection;

    public MySQLClient(IPluginConfig pluginConfig) {

        HikariConfig hikariConfig = new HikariConfig();

        hikariConfig.setJdbcUrl("jdbc:mysql://" + pluginConfig.getString("MySQL.host")
                + ":" + pluginConfig.getInt("MySQL.port"));

        hikariConfig.setUsername(pluginConfig.getString("MySQL.user"));
        hikariConfig.setPassword(pluginConfig.getString("MySQL.password"));
        hikariConfig.setMaximumPoolSize(pluginConfig.getInt("MySQL.poolSize"));
        hikariConfig.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        hikariConfig.addDataSourceProperty("cachePrepStmts", "true");
        hikariConfig.addDataSourceProperty("prepStmtCacheSize", "20");
        hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        hikariConfig.addDataSourceProperty("autoReconnect", "true");
        hikariConfig.addDataSourceProperty("databaseName", pluginConfig.getString("MySQL.database"));

        this.hikariDataSource = new HikariDataSource(hikariConfig);

        try {
            this.connection = this.hikariDataSource.getConnection();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }

    }

    public PreparedStatement preparedStatement(String update) {
        try {
            return this.connection.prepareStatement(update);
        } catch (SQLException exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public void update(PreparedStatement preparedStatement) {
        try {
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    public void update(PreparedStatement preparedStatement, IMySQLCallback<ResultSet> callback) {
        try {
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet != null) {
                callback.success(resultSet);
                preparedStatement.close();
                return;
            }
            callback.failed(new NullPointerException());
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    public void close() {
        try {
            if(!isConnected()) {
                this.connection.close();
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    public boolean isConnected() {
        return connection != null && this.hikariDataSource != null;
    }

}
