package net.rainmc.api.mysql;

import java.sql.SQLException;

public interface IMySQLCallback<T> {

    void success(T result) throws SQLException;

    void failed(Throwable throwable);

}
