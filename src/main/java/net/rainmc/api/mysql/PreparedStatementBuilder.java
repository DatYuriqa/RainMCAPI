package net.rainmc.api.mysql;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PreparedStatementBuilder {

    private int index;

    private final PreparedStatement preparedStatement;

    public PreparedStatementBuilder(String update, MySQLClient mySQLClient) {
        this.preparedStatement = mySQLClient.preparedStatement(update);
        this.index = 1;
    }

    public PreparedStatementBuilder bindString(String s) {
        try {
            this.preparedStatement.setString(this.index, s);
            this.index++;
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return this;
    }

    public PreparedStatementBuilder bindInt(int i) {
        try {
            this.preparedStatement.setInt(this.index, i);
            this.index++;
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return this;
    }

    public PreparedStatementBuilder bindBoolean(boolean b) {
        try {
            this.preparedStatement.setBoolean(this.index, b);
            this.index++;
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return this;
    }

    public PreparedStatement build() {
        return this.preparedStatement;
    }

}
