package net.rainmc.api.mysql;

import net.rainmc.api.AbstractProxyPlugin;
import net.rainmc.api.AbstractSpigotPlugin;
import net.rainmc.api.file.IPluginConfig;
import net.rainmc.service.IProvider;

import java.util.HashMap;

public final class MySQLProvider implements IProvider<MySQLProvider> {

    private final HashMap<String, MySQLClient> connections;

    public MySQLProvider() {
        this.connections = new HashMap<>();
    }

    private boolean exist(AbstractSpigotPlugin plugin, IPluginConfig config) {
        return this.connections.containsKey("MYSQL-" + plugin.getName().toUpperCase() + ":"
                + config.getString("MySQL.database"));
    }

    private boolean exist(AbstractProxyPlugin plugin, IPluginConfig config) {
        return this.connections.containsKey("MYSQL-" + plugin.getProxy().getName().toUpperCase() + ":"
                + config.getString("MySQL.database").toUpperCase());
    }

    public MySQLClient newConnectionPool(AbstractSpigotPlugin plugin, IPluginConfig config) {
        if(!exist(plugin, config)) {
            MySQLClient client = new MySQLClient(config);
            this.connections.put("MYSQL-" + plugin.getName().toUpperCase() +
                    ":" + config.getString("MySQL.database").toUpperCase(), client);
        }
        return this.connections.get("MYSQL-" + plugin.getName().toUpperCase() +
                ":" + config.getString("MySQL.database").toUpperCase());
    }

    public MySQLClient newConnectionPool(AbstractProxyPlugin plugin, IPluginConfig config) {
        if(!exist(plugin, config)) {
            MySQLClient client = new MySQLClient(config);
            this.connections.put("MYSQL-" + plugin.getProxy().getName().toUpperCase() +
                    ":" + config.getString("MySQL.database").toUpperCase(), client);
        }
        return this.connections.get("MYSQL-" + plugin.getProxy().getName().toUpperCase() +
                ":" + config.getString("MySQL.database").toUpperCase());
    }

    @Override
    public String getName() {
        return "mysql";
    }
}
