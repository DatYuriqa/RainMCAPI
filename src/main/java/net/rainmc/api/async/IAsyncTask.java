package net.rainmc.api.async;

public interface IAsyncTask {

    void run();

}
