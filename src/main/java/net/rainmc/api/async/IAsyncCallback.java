package net.rainmc.api.async;

public interface IAsyncCallback {

    void started();

    void completed();

}
