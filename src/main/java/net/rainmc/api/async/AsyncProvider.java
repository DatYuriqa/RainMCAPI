package net.rainmc.api.async;

import net.rainmc.service.IProvider;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public final class AsyncProvider implements IProvider<AsyncProvider> {

    private final ExecutorService executorService;

    public AsyncProvider() {
        this.executorService = Executors.newCachedThreadPool();
    }

    public void newAsyncTask(IAsyncTask task) {

        Future<Thread> future = this.executorService.submit(new Callable<Thread>() {
            @Override
            public Thread call() throws Exception {
                return new Thread(task::run);
            }
        });


    }

    public void newAsyncTask(IAsyncTask task, IAsyncCallback callback) {

        Future<Thread> future = this.executorService.submit(new Callable<Thread>() {
            @Override
            public Thread call() throws Exception {
                return new Thread(() ->{
                    callback.started();
                    task.run();
                    callback.completed();
                });
            }
        });

    }

    @Override
    public String getName() {
        return "async";
    }
}
