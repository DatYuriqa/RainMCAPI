package net.rainmc.api.file;

import net.rainmc.api.enums.ConfigType;

public final class ConfigFactory {

    public IPluginConfig createMySQLConfig(ConfigType type, String path, String fileName) {
        IPluginConfig config;
        if(type.equals(ConfigType.SPIGOT)) {
            config = new SpigotPluginConfig(path, fileName);
        } else {
            config = new ProxyPluginConfig(path, fileName);
        }

        if(!config.exist()) {
            config.set("MySQL.host", "localhost");
            config.set("MySQL.port", 3306);
            config.set("MySQL.poolSize", 5);
            config.set("MySQL.database", "database");
            config.set("MySQL.user", "user");
            config.set("MySQL.password", "password");

            config.create();
        }

        return config;

    }


}
