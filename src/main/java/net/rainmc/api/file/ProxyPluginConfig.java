package net.rainmc.api.file;

import lombok.Getter;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ProxyPluginConfig implements IPluginConfig {

    private final @Getter File path;
    private final @Getter File file;

    private final boolean exists;

    private Configuration configuration;

    public ProxyPluginConfig(String path, String fileName) {
        this.path = new File(path);
        this.file = new File(path + "/" + fileName);

        if(!this.path.exists()) {
            this.path.mkdirs();
        }
        if(!this.file.exists()) {
            this.exists = false;
            try {
                this.file.createNewFile();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        } else {
            this.exists = true;
        }
        try {
            this.configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }


    @Override
    public void set(String path, Object value) {
        this.configuration.set(path.toLowerCase(), value);
    }

    @Override
    public String getString(String path) {
        return this.configuration.getString(path);
    }

    @Override
    public int getInt(String path) {
        return this.configuration.getInt(path.toLowerCase());
    }

    @Override
    public boolean getBoolean(String path) {
        return this.configuration.getBoolean(path.toLowerCase());
    }

    @Override
    public double getDouble(String path) {
        return this.configuration.getDouble(path.toLowerCase());
    }

    @Override
    public long getLong(String path) {
        return this.configuration.getLong(path.toLowerCase());
    }

    @Override
    public List<?> getList(String path) {
        return this.configuration.getList(path.toLowerCase());
    }

    @Override
    public Object get(String path) {
        return this.configuration.get(path);
    }

    @Override
    public boolean exist() {
        return this.exists;
    }

    @Override
    public void create() {
        try {
            if(!this.exists) {
                ConfigurationProvider.getProvider(YamlConfiguration.class).save(this.configuration, this.file);
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void save() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(this.configuration, this.file);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
