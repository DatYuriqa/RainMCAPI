package net.rainmc.api.file;

import lombok.Getter;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class SpigotPluginConfig implements IPluginConfig {

    private final @Getter File path;
    private final @Getter File file;

    private final boolean exists;

    private final YamlConfiguration yamlConfiguration;

    public SpigotPluginConfig(String path, String fileName) {
        this.path = new File(path);
        this.file = new File(path + "/" + fileName);

        if(!this.path.exists()) {
            this.path.mkdirs();
        }
        if(!this.file.exists()) {
            this.exists = false;
            try {
                this.file.createNewFile();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        } else {
            this.exists = true;
        }
        this.yamlConfiguration = YamlConfiguration.loadConfiguration(file);
    }


    @Override
    public void set(String path, Object value) {
        this.yamlConfiguration.set(path.toLowerCase(), value);
    }

    @Override
    public String getString(String path) {
        return this.yamlConfiguration.getString(path.toLowerCase());
    }

    @Override
    public int getInt(String path) {
        return this.yamlConfiguration.getInt(path.toLowerCase());
    }

    @Override
    public boolean getBoolean(String path) {
        return this.yamlConfiguration.getBoolean(path.toLowerCase());
    }

    @Override
    public double getDouble(String path) {
        return this.yamlConfiguration.getDouble(path.toLowerCase());
    }

    @Override
    public long getLong(String path) {
        return this.yamlConfiguration.getLong(path.toLowerCase());
    }

    @Override
    public List<?> getList(String path) {
        return this.yamlConfiguration.getList(path.toLowerCase());
    }

    @Override
    public Object get(String path) {
        return this.yamlConfiguration.get(path);
    }

    @Override
    public boolean exist() {
        return this.exists;
    }

    @Override
    public void create() {
        try {
            if(!this.exists) {
                this.yamlConfiguration.save(this.file);
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void save() {
        try {
            this.yamlConfiguration.save(this.file);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
