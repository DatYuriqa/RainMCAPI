package net.rainmc.api.file;

import java.util.List;

public interface IPluginConfig {

    void set(String path, Object value);

    String getString(String path);

    int getInt(String path);

    boolean getBoolean(String path);

    double getDouble(String path);

    long getLong(String path);

    List<?> getList(String path);

    Object get(String path);

    boolean exist();

    void create();

    void save();

}
