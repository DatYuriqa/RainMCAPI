package net.rainmc.api.file;

import net.rainmc.api.enums.ConfigType;
import net.rainmc.service.IProvider;

public final class ConfigProvider implements IProvider<ConfigProvider> {

    private final ConfigFactory factory;

    public ConfigProvider() {
        this.factory = new ConfigFactory();
    }

    public IPluginConfig createConfig(ConfigType type, String path, String fileName) {
        if(type.equals(ConfigType.SPIGOT)) {
            return new SpigotPluginConfig(path, fileName);
        } else {
            return new ProxyPluginConfig(path, fileName);
        }
    }

    public IPluginConfig loadConfig(ConfigType type, String path, String fileName) {
        return createConfig(type, path, fileName);
    }

    public ConfigFactory factory() {
        return this.factory;
    }

    @Override
    public String getName() {
        return "config";
    }


}
