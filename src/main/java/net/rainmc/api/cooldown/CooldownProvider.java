package net.rainmc.api.cooldown;

import net.rainmc.service.IProvider;
import net.rainmc.service.ServiceProvider;

import java.util.ArrayList;
import java.util.UUID;

public final class CooldownProvider implements IProvider<CooldownProvider> {

    private final ServiceProvider serviceProvider;

    private final ArrayList<String> activeCooldown;

    public CooldownProvider(ServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;
        this.activeCooldown = new ArrayList<>();
    }

    public void newCooldown(UUID uuid, String reason, int seconds) {
        this.activeCooldown.add(uuid + ":" + reason);
        Cooldown cooldown = new Cooldown(this.serviceProvider);
        cooldown.fire(() ->{
            this.activeCooldown.add(uuid + ":" + reason);
        }, seconds);
    }

    public void newAsyncCooldown(UUID uuid, String reason, int seconds) {
        this.activeCooldown.add(uuid + ":" + reason);
        Cooldown cooldown = new Cooldown(this.serviceProvider);
        cooldown.fireAsync(() ->{
            this.activeCooldown.add(uuid + ":" + reason);
        }, seconds);
    }

    public boolean hasCooldown(UUID uuid, String reason) {
        return this.activeCooldown.contains(uuid + ":" + reason);
    }

    @Override
    public String getName() {
        return "cooldown";
    }
}
