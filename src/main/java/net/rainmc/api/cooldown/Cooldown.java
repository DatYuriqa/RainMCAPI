package net.rainmc.api.cooldown;

import net.rainmc.api.scheduler.SchedulerService;
import net.rainmc.service.ServiceProvider;

public class Cooldown {

    private final SchedulerService schedulerService;

    public Cooldown(ServiceProvider serviceProvider) {
        this.schedulerService = (SchedulerService) serviceProvider.getService("schedulder");;
    }

    public void fire(Runnable runnable, int seconds) {
        this.schedulerService.delay(runnable, seconds);
    }

    public void fireAsync(Runnable runnable, int seconds) {
        this.schedulerService.delayAsync(runnable, seconds);
    }

}
