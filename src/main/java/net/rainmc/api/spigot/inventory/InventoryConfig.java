package net.rainmc.api.spigot.inventory;

import net.rainmc.api.AbstractSpigotPlugin;
import net.rainmc.api.enums.LanguageType;
import net.rainmc.api.file.IPluginConfig;
import net.rainmc.api.file.SpigotPluginConfig;

import java.util.HashMap;
public class InventoryConfig {

    private final HashMap<String, Object> cache;

    private final IPluginConfig config;

    private final String inventory;

    public InventoryConfig(AbstractSpigotPlugin plugin, String fileName) {
        this.config = new SpigotPluginConfig(plugin.getDataFolder() + "/inventories", fileName);
        this.cache = new HashMap<>();
        this.inventory = fileName.replace(".yml", "");

        for(LanguageType value : LanguageType.values()) {
            this.config.set("config." + this.inventory + ".inventory.name." + value.getLang(), "Inventory Title");
        }

        this.config.set("config." + this.inventory + ".inventory.size", 9);
        this.config.set("config." + this.inventory + ".inventory.fillpains", false);
        this.config.set("config." + this.inventory + ".inventory.fillpains.id", 9);

        this.config.create();

    }

    public void createItem(String itemName) {

        this.config.set("config." + this.inventory + ".items." + itemName + ".inventory.slot", 0);

        for(LanguageType values : LanguageType.values()) {
            this.config.set("config." + this.inventory + ".items." + itemName + ".name." + values.getLang(), "Name");
            this.config.set("config." + this.inventory + ".items." + itemName + ".lore." + values.getLang(),
                    new String[]{"Line1", "Line2"});
        }

        this.config.set("config." + this.inventory + ".items." + itemName + ".material", "GRASS");
        this.config.set("config." + this.inventory + ".items." + itemName + ".id", 1);
        this.config.set("config." + this.inventory + ".items." + itemName + ".amount", 1);
        this.config.set("config." + this.inventory + ".items." + itemName + ".unbreakable", false);


        this.config.save();

    }

    public String getString(String path, String language) {
        if(!this.cache.containsKey(path)) {
            this.cache.put(path, getFromFile("config." + this.inventory + "." + path + "." + language));
        }
        return (String)this.cache.get(path);
    }

    public String getString(String path) {
        if(!this.cache.containsKey(path)) {
            this.cache.put(path, getFromFile("config." + this.inventory + "." + path));
        }
        return (String)this.cache.get(path);
    }

    public int getInt(String path) {
        if(!this.cache.containsKey(path)) {
            this.cache.put(path, getFromFile("config." + this.inventory + "." + path));
        }
        return (int)this.cache.get(path);
    }

    public boolean getBoolean(String path) {
        if(!this.cache.containsKey(path)) {
            this.cache.put(path, getFromFile("config." + this.inventory + "." + path));
        }
        return (boolean)this.cache.get(path);
    }

    public String[] getStringArray(String path) {
        if(!this.cache.containsKey(path)) {
            this.cache.put(path, getFromFile("config." + this.inventory + "." + path));
        }
        return (String[]) this.cache.get(path);
    }



    public Object getFromFile(String path) {
        return this.config.get(path);
    }

}
