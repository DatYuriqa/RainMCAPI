package net.rainmc.api.spigot.inventory;

import lombok.Getter;
import lombok.Setter;
import net.rainmc.api.enums.LanguageType;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.HashMap;

@Getter @Setter
public class ItemDocument {

    private HashMap<LanguageType, String> names;

    private HashMap<LanguageType, String[]> lore;

    private Material material;

    private int amount;
    private int id;
    private int inventorySlot;

    private boolean unbreakable;

}
