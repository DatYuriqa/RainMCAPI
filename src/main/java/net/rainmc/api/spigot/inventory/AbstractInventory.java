package net.rainmc.api.spigot.inventory;

import lombok.Getter;
import net.rainmc.api.AbstractSpigotPlugin;
import net.rainmc.api.enums.LanguageType;
import net.rainmc.api.player.PlayerProvider;
import net.rainmc.api.player.documents.PlayerLanguageDocument;
import net.rainmc.api.player.player.LanguagePlayer;
import net.rainmc.plugin.SpigotService;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;

public abstract class AbstractInventory implements Listener {

    private final @Getter InventoryConfig config;

    private final HashMap<UUID, Inventory> cache;
    private final HashMap<String, ItemDocument> itemCache;

    public AbstractInventory(AbstractSpigotPlugin plugin, String inventoryNameId) {
        this.config = new InventoryConfig(plugin, inventoryNameId);
        Bukkit.getPluginManager().registerEvents(this, plugin);
        this.cache = new HashMap<>();
        this.itemCache = new HashMap<>();
    }

    public void openInventory(Player player, Runnable runnable) {
        if(!this.cache.containsKey(player.getUniqueId())) {

            PlayerProvider playerProvider = (PlayerProvider) SpigotService.instance.getServiceProvider().getProvider("player");
            LanguagePlayer languagePlayer = playerProvider.getLanguagePlayer(player.getUniqueId());

            Inventory inventory = Bukkit.createInventory(null, this.config.getInt("inventory.size")
                    , this.config.getString("inventory.name", languagePlayer.getLanguage()));
            this.cache.put(player.getUniqueId(), inventory);
        }

        runnable.run();
        player.openInventory(this.cache.get(player.getUniqueId()));
    }

    public void openInventory(Player player) {
        if(!this.cache.containsKey(player.getUniqueId())) {

            PlayerProvider playerProvider = (PlayerProvider) SpigotService.instance.getServiceProvider().getProvider("player");
            LanguagePlayer languagePlayer = playerProvider.getLanguagePlayer(player.getUniqueId());

            Inventory inventory = Bukkit.createInventory(null, this.config.getInt("inventory.size")
                    , this.config.getString("inventory.name", languagePlayer.getLanguage()));
            this.cache.put(player.getUniqueId(), inventory);

            this.itemCache.forEach((s, itemDocument) -> {

                ItemStack itemStack = new ItemStack(itemDocument.getMaterial()
                        , itemDocument.getAmount(), (short)itemDocument.getId());
                ItemMeta itemMeta = itemStack.getItemMeta();

                if(!itemDocument.getNames().isEmpty()) {
                    itemMeta.setDisplayName(itemDocument.getNames().get(languagePlayer.getLanguageType()));
                }
                if(!itemDocument.getLore().isEmpty()) {
                    itemMeta.setLore(Arrays.asList(itemDocument.getLore().get(languagePlayer.getLanguageType())));
                }

                itemMeta.spigot().setUnbreakable(itemDocument.isUnbreakable());

                itemStack.setItemMeta(itemMeta);

                inventory.setItem(itemDocument.getInventorySlot(), itemStack);

            });


        }

        player.openInventory(this.cache.get(player.getUniqueId()));
    }

    public void addItem(String itemPath) {
        ItemDocument document = new ItemDocument();

        if(this.config.getString(itemPath) == null) {
            String itemName = itemPath.replace("items.", "");
            this.config.createItem(itemName);
        }

        HashMap<LanguageType, String> names = new HashMap<>();
        HashMap<LanguageType, String[]> lore = new HashMap<>();

        for(LanguageType value : LanguageType.values()) {
            names.put(value, this.config.getString(itemPath + ".name." + value.getLang()));

            lore.put(value, this.config.getStringArray(itemPath + ".lore." + value.getLang()));
        }

        document.setNames(names);
        document.setLore(lore);

        document.setInventorySlot(this.config.getInt(itemPath + ".inventory.slot"));
        document.setId(this.config.getInt(itemPath + ".id"));
        document.setAmount(this.config.getInt(itemPath + ".amount"));
        document.setUnbreakable(this.config.getBoolean(itemPath + ".unbreakable"));
        document.setMaterial(Material.valueOf(itemPath + ".material"));

        this.itemCache.put(itemPath, document);

    }



    @EventHandler
    public void inventoryClickEvent(InventoryClickEvent event) {

    }

}
