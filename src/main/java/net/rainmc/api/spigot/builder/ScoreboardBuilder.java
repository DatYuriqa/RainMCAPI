package net.rainmc.api.spigot.builder;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import java.util.Objects;

public class ScoreboardBuilder {

    private final Player player;
    private final Scoreboard scoreboard;
    private final Objective objective;

    private int taskId;

    public ScoreboardBuilder(Player player) {
        this.player = player;
        this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        this.objective = scoreboard.registerNewObjective("Scoreboard", "Builder");
        this.objective.setDisplaySlot(DisplaySlot.SIDEBAR);
    }

    public ScoreboardBuilder setBoardTitle(String title) {
        this.objective.setDisplayName(title);
        return this;
    }

    public ScoreboardBuilder addBoardLine(String text, int lineId) {
        this.objective.getScore(text).setScore(lineId);
        return this;
    }

    public ScoreboardBuilder addUpdatableBoardLine(String prefix, String suffix, String entry, int lineId) {
        Team team = scoreboard.registerNewTeam("x" + lineId);
        team.setPrefix(prefix);
        team.setSuffix(suffix);
        team.addEntry(entry);
        this.objective.getScore(entry).setScore(lineId);
        return this;
    }

    public ScoreboardBuilder addBoardTeam(int sortId, String name, String prefix, String suffix, boolean seeFriendlyInvisible, NameTagVisibility nameTagVisibility) {
        Team team = scoreboard.registerNewTeam(sortId + "-" + name);
        team.setPrefix(prefix);
        team.setSuffix(suffix);
        team.setCanSeeFriendlyInvisibles(seeFriendlyInvisible);
        team.setNameTagVisibility(nameTagVisibility);
        return this;
    }

    public void create() {
        this.player.setScoreboard(scoreboard);
    }

    public void update(String prefix, String suffix, int lineId) {
        if(this.player.getScoreboard() == null | Objects.requireNonNull(this.player.getScoreboard()).getObjective(DisplaySlot.SIDEBAR) == null)
            return;
        if(prefix != null && !prefix.isEmpty())
            this.player.getScoreboard().getTeam("x" + lineId).setPrefix(prefix);
        if(suffix != null && !suffix.isEmpty())
            this.player.getScoreboard().getTeam("x" + lineId).setSuffix(suffix);
    }

}
