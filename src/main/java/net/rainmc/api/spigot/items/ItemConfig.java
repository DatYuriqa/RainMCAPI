package net.rainmc.api.spigot.items;

import net.rainmc.api.AbstractSpigotPlugin;
import net.rainmc.api.enums.LanguageType;
import net.rainmc.api.file.IPluginConfig;
import net.rainmc.api.file.SpigotPluginConfig;

import java.util.HashMap;

public class ItemConfig {

    private final HashMap<String, Object> cache;

    private final IPluginConfig config;

    public ItemConfig(AbstractSpigotPlugin plugin, String fileName) {
        this.config = new SpigotPluginConfig(plugin.getDataFolder() + "/items", fileName);
        this.cache = new HashMap<>();
    }

    public void createItem(String itemName) {

        this.config.set("config.items." + itemName + ".slot", 0);

        for(LanguageType values : LanguageType.values()) {
            this.config.set("config.items." + itemName + ".name." + values.getLang(), "Name");
            this.config.set("config.items." + itemName + ".lore." + values.getLang(),
                    new String[]{"Line1", "Line2"});
        }

        this.config.set("config.items." + itemName + ".material", "GRASS");
        this.config.set("config.items." + itemName + ".id", 1);
        this.config.set("config.items." + itemName + ".amount", 1);
        this.config.set("config.items." + itemName + ".unbreakable", false);


        this.config.save();

    }

    public String getString(String path, String language) {
        if(!this.cache.containsKey(path)) {
            this.cache.put(path, getFromFile("config." + path + "." + language));
        }
        return (String)this.cache.get(path);
    }

    public String getString(String path) {
        if(!this.cache.containsKey(path)) {
            this.cache.put(path, getFromFile("config." + path));
        }
        return (String)this.cache.get(path);
    }

    public int getInt(String path) {
        if(!this.cache.containsKey(path)) {
            this.cache.put(path, getFromFile("config." + path));
        }
        return (int)this.cache.get(path);
    }

    public boolean getBoolean(String path) {
        if(!this.cache.containsKey(path)) {
            this.cache.put(path, getFromFile("config." + path));
        }
        return (boolean)this.cache.get(path);
    }

    public String[] getStringArray(String path) {
        if(!this.cache.containsKey(path)) {
            this.cache.put(path, getFromFile("config." + path));
        }
        return (String[]) this.cache.get(path);
    }



    public Object getFromFile(String path) {
        return this.config.get(path);
    }

}
