package net.rainmc.api.spigot.items;

import net.rainmc.api.player.player.LanguagePlayer;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class ItemBuilder {

    private final ItemStack itemStack;
    private final ItemMeta itemMeta;

    private final ItemConfig config;

    private final String path;

    private final LanguagePlayer languagePlayer;

    public ItemBuilder(ItemConfig config, String path, LanguagePlayer languagePlayer) {
        this.config = config;
        this.path = path;
        this.languagePlayer = languagePlayer;

        this.itemStack = new ItemStack(Material.valueOf(this.config.getString(path + ".material"))
                , this.config.getInt(path + ".amount"), (short) this.config.getInt(path + ".id"));
        this.itemMeta = this.itemStack.getItemMeta();

    }

    public ItemBuilder addDisplayName() {
        this.itemMeta.setDisplayName(this.config.getString(this.path + ".name", this.languagePlayer.getLanguage()));
        return this;
    }

    public ItemBuilder addLore() {
        this.itemMeta.setLore(Arrays.asList(this.config.getStringArray(this.path + ".lore." + this.languagePlayer.getLanguage())));
        return this;
    }

    public ItemBuilder setUnbreakable() {
        this.itemMeta.spigot().setUnbreakable(this.config.getBoolean(this.path + ".unbreakable"));
        return this;
    }

    public ItemStack build() {
        this.itemStack.setItemMeta(this.itemMeta);
        return this.itemStack;
    }

}
