package net.rainmc.api.spigot.events;

import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public final class PlayerNickEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private final @Getter Player player;
    private final @Getter String nick;

    public PlayerNickEvent(Player player, String nick) {
        this.player = player;
        this.nick = nick;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}
