package net.rainmc.api;

import lombok.Getter;
import net.md_5.bungee.api.plugin.Plugin;
import net.rainmc.plugin.ProxyService;
import net.rainmc.plugin.manager.ProxyPluginManager;
import net.rainmc.service.ServiceProvider;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class AbstractProxyPlugin extends Plugin {

    private @Getter ServiceProvider serviceProvider;
    private @Getter ProxyPluginManager pluginManager;

    private boolean enabled = false;

    @Override
    public void onEnable() {
        if(!this.enabled) {
            this.enabled = true;

            this.serviceProvider = ProxyService.instance.getServiceProvider();;
            this.pluginManager = ProxyService.instance.getPluginManager();

            this.pluginManager.register(this);

            ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
            executorService.schedule(this::enable, 2, TimeUnit.SECONDS);
        }
        super.onEnable();

    }

    @Override
    public void onDisable() {
        this.pluginManager.unregister(this);
        disable();
        super.onDisable();
    }

    public void enable() {

    }

    public void disable() {

    }

}
