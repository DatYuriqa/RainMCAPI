package net.rainmc.api.player;

import net.rainmc.api.player.documents.PlayerLanguageDocument;
import net.rainmc.api.player.player.ClientPlayer;
import net.rainmc.api.player.player.LanguagePlayer;
import net.rainmc.api.player.player.PermissionPlayer;
import net.rainmc.service.IProvider;
import net.rainmc.service.ServiceProvider;

import java.util.HashMap;
import java.util.UUID;

public final class PlayerProvider implements IProvider<PlayerProvider> {

    private final ServiceProvider serviceProvider;

    private final HashMap<UUID, PermissionPlayer> permissionPlayers;
    private final HashMap<UUID, LanguagePlayer> languagePlayers;
    private final HashMap<UUID, ClientPlayer> clientPlayers;

    public PlayerProvider(ServiceProvider serviceProvider) {
        this.serviceProvider = serviceProvider;
        this.permissionPlayers = new HashMap<>();
        this.languagePlayers = new HashMap<>();
        this.clientPlayers = new HashMap<>();
    }

    public PermissionPlayer getPermissionPlayer(UUID uuid) {
        if(!this.permissionPlayers.containsKey(uuid)) {
            PermissionPlayer permissionPlayer = new PermissionPlayer(uuid);
            this.permissionPlayers.put(uuid, permissionPlayer);
        }
        return this.permissionPlayers.get(uuid);
    }

    public LanguagePlayer getLanguagePlayer(UUID uuid) {
        if(!this.languagePlayers.containsKey(uuid)) {
            LanguagePlayer languagePlayer = new LanguagePlayer(uuid, this.serviceProvider);
            this.languagePlayers.put(uuid, languagePlayer);
        }
        return this.languagePlayers.get(uuid);
    }

    public ClientPlayer getClientPlayer(UUID uuid) {
        if(!this.clientPlayers.containsKey(uuid)) {
            ClientPlayer clientPlayer = new ClientPlayer(uuid);
            this.clientPlayers.put(uuid, clientPlayer);
        }
        return this.clientPlayers.get(uuid);
    }

    @Override
    public String getName() {
        return "player";
    }
}
