package net.rainmc.api.player.documents;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter @Setter
public class PlayerLanguageDocument {

    private UUID uuid;

    private String lang;

}
