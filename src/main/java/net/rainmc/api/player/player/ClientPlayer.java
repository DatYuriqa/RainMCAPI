package net.rainmc.api.player.player;

import net.rainmc.api.enums.ClientType;

import java.util.UUID;

public class ClientPlayer {

    private ClientType clientType;

    public ClientPlayer(UUID uuid) {
        this.clientType = ClientType.VANILLA;
    }

    public void setClientType(ClientType type) {
        this.clientType = type;
    }

    public ClientType getClientType() {
        return this.clientType;
    }

}
