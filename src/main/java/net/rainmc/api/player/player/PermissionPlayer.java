package net.rainmc.api.player.player;

import de.dytanic.cloudnet.api.CloudAPI;
import de.dytanic.cloudnet.api.player.PermissionProvider;
import de.dytanic.cloudnet.lib.player.OfflinePlayer;
import de.dytanic.cloudnet.lib.player.permission.PermissionEntity;
import net.rainmc.api.enums.RankType;

import java.util.UUID;

public class PermissionPlayer {

    private final UUID uuid;

    private final PermissionEntity permissionEntity;

    public PermissionPlayer(UUID uuid) {
        this.uuid = uuid;

        OfflinePlayer offlinePlayer = CloudAPI.getInstance().getOfflinePlayer(uuid);
        this.permissionEntity = offlinePlayer.getPermissionEntity();
    }

    public RankType getRank() {
        for (RankType value : RankType.values()) {
            if(PermissionProvider.getGroupName(uuid).equals(value.getName().toLowerCase())) {
                return value;
            } else {
                return RankType.PLAYER;
            }
        }
        return RankType.PLAYER;
    }

    public boolean hasRank(RankType type) {
        return permissionEntity.isInGroup(type.getName().toLowerCase());
    }

    public boolean hasMinRank(RankType type) {
        return getRank().getRankId() >= type.getRankId();
    }

    public boolean isTeam() {
        return hasMinRank(RankType.BUILDER);
    }


}
