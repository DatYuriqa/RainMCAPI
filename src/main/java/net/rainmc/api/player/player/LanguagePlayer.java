package net.rainmc.api.player.player;

import net.rainmc.api.enums.LanguageType;
import net.rainmc.api.message.MySQLMessageService;
import net.rainmc.api.player.documents.PlayerLanguageDocument;
import net.rainmc.service.ServiceProvider;

import java.util.UUID;

public class LanguagePlayer {

    private final PlayerLanguageDocument document;

    public LanguagePlayer(UUID uuid, ServiceProvider serviceProvider) {
        MySQLMessageService service = (MySQLMessageService) serviceProvider.getService("mysql_message");
        this.document = service.getPlayer(uuid);
    }

    public void setLanguage(LanguageType type) {
        this.document.setLang(type.getLang());
    }

    public LanguageType getLanguageType() {
        for(LanguageType value : LanguageType.values()) {
            if(this.document.getLang().equals(value.getLang())) {
                return value;
            } else {
                return null;
            }
        }
        return null;
    }

    public String getLanguage() {
        return this.document.getLang();
    }


}
