package net.rainmc.api;

import lombok.Getter;
import net.rainmc.plugin.SpigotService;
import net.rainmc.plugin.manager.SpigotPluginManager;
import net.rainmc.service.ServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class AbstractSpigotPlugin extends JavaPlugin {

    private @Getter ServiceProvider serviceProvider;
    private @Getter SpigotPluginManager pluginManager;

    private boolean enabled = false;

    @Override
    public void onEnable() {
        if(!this.enabled) {
            this.enabled = true;

            this.serviceProvider = SpigotService.instance.getServiceProvider();
            this.pluginManager = SpigotService.instance.getPluginManager();

            this.pluginManager.register(this);

            ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
            executorService.schedule(this::enable, 2, TimeUnit.SECONDS);
        }

        super.onEnable();

    }

    @Override
    public void onDisable() {
        this.pluginManager.unregister(this);
        disable();
        super.onDisable();
    }

    public void enable() {

    }

    public void disable() {

    }

}
